import User from '../models/User.js'
import { StatusCodes } from 'http-status-codes'
import { BadRequestError, NoAthenticatedError } from '../errors/index.js'

const register = async (req, res) => {
  const { name, email, password, passwordRepeat } = req.body

  if (!name || !email || !password ) {
    throw new BadRequestError('Por favor, completa todos los campos')
  }
  
  const userAlreadyExists = await User.findOne({ email })
  if (userAlreadyExists) {
    throw new BadRequestError('El email introducido se encuentra en uso')
  }
  const user = await User.create({ name, email, password })

  const token = user.createJWT()
  res.status(StatusCodes.CREATED).json({
    user: {
      email: user.email,
      lastName: user.lastName,
      name: user.name,
      phone: user.phone,
      city: user.city
    },
    token,
  })
}
const login = async (req, res) => {
  const { email, password } = req.body
  if (!email || !password) {
    throw new BadRequestError('Por favor, completa todos los campos')
  }
  const user = await User.findOne({ email }).select('+password')
  if (!user) {
    throw new NoAthenticatedError('Credenciales incorrectas')
  }

  const isPasswordCorrect = await user.comparePassword(password)
  if (!isPasswordCorrect) {
    throw new NoAthenticatedError('Credenciales incorrectas')
  }
  const token = user.createJWT()
  user.password = undefined
  res.status(StatusCodes.OK).json({ user, token })
}


const updateUser = async (req, res) => {
  const { email, name, lastName, phone, city} = req.body
  if (!email || !name || !lastName ) {
    throw new BadRequestError('Por favor, completa todos los campos')
  }
  const user = await User.findOne({ _id: req.user.userId })

  user.email = email
  user.name = name
  user.lastName = lastName
  user.city = city
  user.phone = phone

  await user.save()

  const token = user.createJWT()

  res.status(StatusCodes.OK).json({ user, token })
}

export { register, login, updateUser }
