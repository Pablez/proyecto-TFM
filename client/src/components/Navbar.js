import Wrapper from '../assets/wrappers/Navbar'
import { FaAlignLeft } from 'react-icons/fa'
import { FiLogOut } from 'react-icons/fi';
import { useAppContext } from '../context/appContext'
import Logo from '../components/Logo'
import { useState } from 'react'
import { Link } from 'react-router-dom'

const Navbar = () => {
  const { toggleSidebar, logoutUser, user } = useAppContext()
  const [showLogout, setShowLogout] = useState(false)

  return (
    <Wrapper>
      <div className='nav-center'>
        <button type='button' className='toggle-btn' onClick={toggleSidebar}>
          <FaAlignLeft />
        </button>
        <div>
          <Logo />
          <Link to="/"><h3 className='logo-text'>Bienvenid@ de nuevo, {user.name}</h3></Link> 
        </div>
        <div className='btn-container'>
          <button
            type='button'
            className='btn'
            onClick={() => setShowLogout(!showLogout)}
          >
            <FiLogOut />
            Cerrar Sesión
          </button>
          <div className={showLogout ? 'dropdown show-dropdown' : 'dropdown'}>
            <button type='button' className='dropdown-btn' onClick={logoutUser}>
              ¿Confirmar?
            </button>
          </div>
        </div>
      </div>
    </Wrapper>
  )
}

export default Navbar
