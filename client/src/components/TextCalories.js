
const TextCal = ({imc, kcal, obj}) =>{
    const textStyle = {
        textAlign: 'justify',
        paddingTop:'20px'
      }
      
    return (
        <div style={textStyle}>
              <h5>IMC : {imc} </h5>
              <h5>Kilocalorías diarias mantenimiento: {kcal}</h5>
              {obj === 1 ? (
              <h5>Kilocalorías recomendadas para bajar de peso: {kcal - 300}</h5>
              ):(
              <h5>Kilocalorías recomendadas para subir de peso: {kcal + 300}</h5>
              )}
            </div>
    )
}

export default TextCal