import React from 'react'
import fondo from "../assets/images/fondo1.jpg"



const MountainBackground = () => {

    return (
            <img src={fondo} className="fondo" alt="Fondo de montañas" style={{position:"absolute", zIndex:"-2"}}></img>
    ) 
 }   

 export default MountainBackground