const FormRow = ({ type, name, value, handleChange, labelText, disabled=false }) => {
  return (
    <div className='form-row'>
      <label htmlFor={name} className='form-label'>
        {labelText || name}
      </label>
      <input
        type={type}
        value={value}
        name={name}
        disabled={disabled}
        onChange={handleChange}
        className='form-input'
      />
    </div>
  )
}

export default FormRow
