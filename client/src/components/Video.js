import React from 'react'
import ReactPlayer from 'react-player'
import styles from "../assets/modules/Video.module.css"



const Video = (props) => {

    return (
        <div >
            <ReactPlayer className={styles.video} url={props.source} controls={props.controls} playing={props.playing} loop={props.loop}></ReactPlayer>         
        </div>
    ) 
 }   

 export default Video