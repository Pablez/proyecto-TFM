import styles from "../assets/modules/ExerciseCard.module.css"
import { Link } from "react-router-dom"
import  predefined from "../assets/images/pesas.jpg"

const ExerciseCard = ({ exercise }) => {
    var imagen = exercise.imagen
    if (imagen===null || imagen===""){
        imagen = predefined
    }
    
    return (
        <li className={styles.exerciseCard}>
            <Link to={"/rutina/" + exercise.id}>
                <img 
                    className={styles.exerciseImage} 
                    src={imagen} 
                    alt={exercise.nombre} 
                    width={300} 
                    height={200} 
                    />
                <div>
                    <h5>{exercise.nombre}</h5>
                </div>
            </Link>
        </li>
    )
}

export default ExerciseCard