import { exercisesSeguridad } from "../utils/exercises.json" //en caso de fallo al recuperar datos de API
import ExerciseCard from "./ExerciseCard"
import styles from "../assets/modules/ExerciseGrid.module.css"
import { useEffect, useState } from "react";


const ExerciseGrid= () => {

    const URI = `http://demo6197451.mockable.io/`

    const [ exercises, setExercises ] = useState(null);

    useEffect(() => {
        fetch(URI, {
            mode: "no-cors",
})
        .then((res) => res.json())
        .then((data) => (
            setExercises(data.exercises)))
            .catch((e) => {
                setExercises(exercisesSeguridad)
            })
    });

    if (!exercises) return null;

    return (
        <ul className={styles.exerciseGrid}>
            {exercises.map ((exercise) => (
                <ExerciseCard key={exercise.id} exercise={exercise}/>
            ))
            }
        </ul>
    )
}

export default ExerciseGrid