import { Tooltip } from "@mui/material"
import { IconButton } from "@mui/material"
import { FaInfoCircle } from "react-icons/fa"

const ToolTipCalc = () => {


  const text = "A continuación encontrarás los valores medios de las necesidades calóricas en función de las características personales y de los diferentes niveles de actividad física, para que puedas orientarte mejor a la hora de diseñar tu plan de entrenamiento/nutrición. \n No olvides que se trata de un cálculo aproximado, y lo ideal es ponerse en manos de un especialista."

  return (
      <Tooltip title={text}>
        <IconButton>
          <FaInfoCircle/>
        </IconButton>
      </Tooltip>

  )
}

export default ToolTipCalc