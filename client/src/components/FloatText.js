import styles from "../assets/modules/FloatText.module.css"
import Video from "./Video";
import vid from '../assets/images/video.mp4'
import { Link } from 'react-router-dom'

const FloatText = () => {
    const URL = vid
    return (
    <>
    <div className={styles.lineUp}></div>
    <div className={styles.container}>
        <div className={styles.FloatText}>
            <Video source={URL} controls={false} playing={true} loop={true} className="videoMain"></Video>
        </div>
        <div><Link to='/register' className='btn btn-hero'>
            Empieza hoy tu viaje
          </Link></div>
    </div>
    </>
)};

export default FloatText;