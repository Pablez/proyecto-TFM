import logo from '../assets/images/logoDumbell.svg'

const Logo = () => {
  return <img src={logo} width="200" height="60" alt='logoPrincipal' className='logo' />
}

export default Logo
