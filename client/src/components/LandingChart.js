import React from 'react';
import { PieChart, Pie, Legend, Cell } from 'recharts';
import styles from "../assets/modules/Video.module.css"

const LandingChart = () => {

    // Sample data
    const data = [
        { name: 'Más de 3 horas', porcen: 4 },
        { name: '3 horas', porcen: 10 },
        { name: '2 horas', porcen: 20 },
        { name: '1 horas', porcen: 24 },
        { name: 'No realiza actividad física', porcen: 42 }
    ];


    return (
        <PieChart width={500} height={500} className={styles.video}>

            <Legend verticalAlign='top' align='center' fill='##FFFFFF' type='square'></Legend>
            <Pie data={data} dataKey="porcen" outerRadius={150} label>
                <Cell fill="#560eff" />
                <Cell fill="#584192" />
                <Cell fill="#3b2f53" />
                <Cell fill="#312c3d" />
                <Cell fill="#585f66" />
            </Pie>
        </PieChart>
    );
}

export default LandingChart;