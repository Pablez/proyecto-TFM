import React from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  Label
} from "recharts";


const BarChart = ({ peso, imc, obj }) => {

  var graphPeso = peso;
  var minPeso = graphPeso-25;
  var maxPeso = parseInt(graphPeso)+25;

  const data = [
    {
      name: "1-2",
      BajarPeso: graphPeso,
      SubirPeso: graphPeso

    },
    {
      name: "3-4",
      BajarPeso: graphPeso - 2,
      SubirPeso: parseInt(graphPeso) + 2

    },
    {
      name: "5-6",
      BajarPeso: graphPeso - 4,
      SubirPeso: parseInt(graphPeso) + 4
    },
    {
      name: "7-8",
      BajarPeso: graphPeso - 6,
      SubirPeso: parseInt(graphPeso) + 5
    },
    {
      name: "9-10",
      BajarPeso: graphPeso - 8,
      SubirPeso: parseInt(graphPeso) + 6
    }
  ];

  return (
    <LineChart
      width={500}
      height={300}
      data={data}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 5
      }}
    >
      <CartesianGrid strokeDasharray="4 4" />
      <XAxis dataKey="name" >
        <Label value="Semanas" offset={0} position="insideBottom" />
      </XAxis>
      <YAxis domain={[minPeso, maxPeso]} label={{ value: 'Peso', angle: -90, position: 'insideLeft' }} />
      <Tooltip />
      <Legend verticalAlign="top" height={36} />
      {obj === 1 ? (
        <Line
          type="monotone"
          dataKey="BajarPeso"
          stroke="#8884d8"
          activeDot={{ r: 3 }}
        />
      ) : (
        <Line
          type="monotone"
          dataKey="SubirPeso"
          stroke="#82ca9d"
          activeDot={{ r: 3 }} />
      )
      }
    </LineChart>

  );
}

export default BarChart