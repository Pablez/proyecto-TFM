import { Link } from 'react-router-dom'

const TextFaqs = (props) => {

    var texto = ""
    switch (props.param) {
        case 0:
            texto = (
                <>
                    <p>Se ha demostrado que la actividad física regular ayuda a prevenir y controlar las enfermedades no transmisibles, como las enfermedades cardíacas, los accidentes cerebrovasculares, la diabetes y varios tipos de cáncer. También ayuda a prevenir la hipertensión, a mantener un peso corporal saludable y puede mejorar la salud mental, la calidad de vida y el bienestar.</p>
                </>
            )
            break
        case 1:
            texto = (
                <>
                    <p>La calculadora de calorías Harris-Benedict se basa en cinco parámetros diferentes para calcular el número de calorías recomendado: Sexo, Altura, Peso, Edad y Actividad física.</p>
                    <p>Con estos párametros la calculadora es capaz de calcular la Tasa de Metabolismo Basal (TMB), es decir, la cantidad mínima de energía que necesita tu cuerpo para funcionar. Nunca debemos ingerir menos cantidad de calorías de las que marca la tasa metabólica. La TMB se calcula siguiendo las siguientes ecuaciones:</p>
                    <ul>
                        <li>TMB Mujer = 655 + (9,6 * P) + (1,8 * A) – (4,7 * E)</li>
                        <li>TMB Hombre = 66 + (13,7 * P) + (5 * A) – (6,8 * E)</li>
                    </ul>
                    <p>Una vez obtenida la Tasa Metabólica Basal, el método Harris-Benedict pone en relación el último de los valores, el nivel de actividad, para obtener la cantidad recomendada de calorías que hacen falta tanto para mantener el peso actual, como para adelgazar o engordar.</p>
                    <p>El gráfico es una estimación de cómo podría verse modificado el peso en caso de seguir una dieta basada en las Kcal recomendadas por la calculadora.</p>
                </>
            );
            break

        case 2:
            texto = (
                <>
                    <p>Siguiendo las pautas de la OMS, las recomendaciones para adultos de 18 a 64 años son las siguientes</p>
                    <ul>
                        <li>- Realizar actividades físicas aeróbicas moderadas durante al menos 150 a 300 minutos,</li>
                        <li>- o actividades físicas aeróbicas intensas durante al menos 75 a 150 minutos; o una combinación equivalente de actividades moderadas e intensas a lo largo de la semana.</li>
                        <li>- Realizar actividades de fortalecimiento muscular moderadas o más intensas que ejerciten todos los grupos musculares principales durante dos o más días a la semana, ya que tales actividades aportan beneficios adicionales para la salud.</li>
                        <li>- Se puede prolongar la actividad física aeróbica moderada más allá de 300 minutos; o realizar actividades físicas aeróbicas intensas durante más de 150 minutos; o una combinación equivalente de actividades moderadas e intensas a lo largo de la semana para obtener beneficios adicionales para la salud.</li>
                        <li>- Se debe limitar el tiempo dedicado a actividades sedentarias. La sustitución del tiempo dedicado a actividades sedentarias por actividades físicas de cualquier intensidad (incluidas las de baja intensidad) es beneficiosa para la salud.</li>
                        <li>- Para ayudar a reducir los efectos perjudiciales de los comportamientos más sedentarios en la salud, todos los adultos y los adultos mayores deberían tratar de incrementar su actividad física moderada a intensa por encima del nivel recomendado.</li>
                    </ul>
                </>
            )
            break
        case 3:
            texto = (
                <>
                    <p>Puedes enviar un correo electrónico a través de la sección de <Link to="/contacto">CONTACTO</Link> </p>
                </>
            )
            break
        case 4:
            texto = (
                <>
                    <p>Si quieres eliminar tu cuenta de usuario puedes hacerlo a través del siguiente <Link to="/borrarCuenta">enlace</Link>.</p>
                </>
            )
            break
        default:
            break
    }

    return (
        <div>{texto}</div>
    )

}

export default TextFaqs