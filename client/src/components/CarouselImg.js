import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import pesas from '../assets/images/pesas.jpg';
import pesas2 from '../assets/images/pesas2.jpg';
import pesas3 from '../assets/images/pesas3.webp';
import pesas4 from '../assets/images/pesas5.jpg';

const CarouselImg = () => {

  const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1,
    slidesToSlide: 1
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1,
    slidesToSlide: 1 
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1 
  }
};

return(
<Carousel
  className='carousel'
  swipeable={false}
  draggable={false}
  showDots={false}
  responsive={responsive}
  ssr={true}
  infinite={true}
  autoPlay={true}
  keyBoardControl={true}
  transitionDuration={500}
  containerClass="carousel-container"
  removeArrowOnDeviceType={["desktop", "tablet", "mobile"]}
  dotListClass="custom-dot-list-style"
  itemClass="carousel-item-padding-40-px"
>
<div><img src={pesas} alt='Hombre corriendo' className='img main-img' width={500} height={500}/></div>
<div><img src={pesas2} alt='Hombre corriendo' className='img main-img' width={500} height={500}/></div>
<div><img src={pesas3} alt='Hombre corriendo' className='img main-img' width={500} height={500}/></div>
<div><img src={pesas4} alt='Hombre corriendo' className='img main-img' width={500} height={500}/></div>

</Carousel>
)
}

export default CarouselImg