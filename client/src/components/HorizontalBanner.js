import React from 'react'
import Wrapper from '../assets/wrappers/HorizontalBanner'
import { FaCalendarWeek } from 'react-icons/fa'
import { FaHeart } from 'react-icons/fa/index.esm'
import { FaHouseUser } from 'react-icons/fa/index.esm'
import { FaDumbbell } from 'react-icons/fa/index.esm'


const HorizontalBanner = () => {

    return (
        <Wrapper>
            <div className='flex-container'>
                <div className='item'>
                    <div><p>Nuevas rutinas todas las semanas</p></div>
                    <div className='icon-container'><FaCalendarWeek className='icon'/></div>
                </div>
                <div className='item'>
                    <div><p>Rutinas adaptadas por dificultad</p></div>
                    <div className='icon-container'><FaHeart className='icon'/></div>
                </div>
                <div className='item'>
                    <div><p>Ahorra tiempo entrenando desde casa</p></div>
                    <div className='icon-container'><FaHouseUser className='icon'/></div>
                </div>
                <div className='item'>
                    <div><p>Ponerse en forma nunca fue tan fácil</p></div>
                    <div className='icon-container'><FaDumbbell className='icon'/></div>
                </div>
            </div>
        </Wrapper>
    )
}

export default HorizontalBanner