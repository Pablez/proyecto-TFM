import {
  Grid,
  Paper,
  TextField,
  Button,
  Select,
  MenuItem,
  RadioGroup,
  Radio,
  Box,
} from "@mui/material";
import React from "react";
import {
  InputLabel,
  FormLabel,
  FormControlLabel,
} from "@mui/material";
import { useState } from 'react'
import calculations from "../utils/calculations";
import ToolTipCalc from "./ToolTipCalc";
import TextCalories from "./TextCalories";
import BarChartComponent from "./BarChart";

const Calculator = () => {
  const paperStyle = {
    padding: 20,
    marginbottom:20,
    width: 280,
    zindex: -2,
    margin: "0 auto",
    marginBottom: "30px"
  };

  const gridStyle = {
    display: "flex",
    flexdirection:"column",
    margin: "0 auto",
    overflow: "auto",
    alignItems: "center",
  }

  const gridItem ={
    display:"flex",
    flex: 1,
  }

  const toolTip={
    textAlign:"center"
  }

  const btnStyle = { margin: "8px 0", backgroundColor: "#17034e"};
  const boxStyle = { paddingTop: "10px" };

  const [nivel, setNivel] = useState("");
  const [edad, setEdad] = useState("");
  const [peso, setPeso] = useState("");
  const [sexo, setSexo] = useState("");
  const [altura, setAltura] = useState("");
  const [objetivo, setObjetivo] = useState("");


  const [calories, setCalories] = useState(0);
  const [imc, setImc] = useState(0);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (edad && nivel && peso && sexo && altura) {
      setImc(calculations.calculateIMC(peso, altura))
      setCalories(calculations.calculateKcal(edad, nivel, peso, sexo, altura));
    }
  };

  return (
    <>
    <div style={toolTip}><ToolTipCalc/></div>
    <div style={gridStyle}>
    <Grid item style={gridItem}>
        {calories ? (
          <BarChartComponent peso={peso} imc={imc} obj={objetivo} />
        ) : <></>
        }
      </Grid>
      <Grid item xs={4} md={4} style={gridItem}>
        <Paper elevation={10} style={paperStyle}>

          <form autoComplete="off" onSubmit={handleSubmit}>
            <TextField
              style={boxStyle}
              label="Edad"
              placeholder="Introduce tu edad"
              fullWidth
              required
              onChange={(e) => setEdad(e.target.value)}
            />
            <TextField
              style={boxStyle}
              label="Altura"
              placeholder="Altura en cm"
              fullWidth
              required
              onChange={(e) => setAltura(e.target.value)}
            />
            <TextField
              style={boxStyle}
              label="Peso"
              placeholder="Peso en kg"
              fullWidth
              required
              onChange={(e) => setPeso(e.target.value)}
            />
            <Box fullWidth style={boxStyle}>
              <InputLabel id="nivel">Nivel de Actividad</InputLabel>
              <Select
                labelId="nivel"
                defaultValue={0}
                fullWidth
                id="nivel"
                label="Nivel de actividad"
                onChange={(e) => setNivel(e.target.value)}
              >
                <MenuItem value={0}>--Seleccionar--</MenuItem>
                <MenuItem value={1}>Sedentario</MenuItem>
                <MenuItem value={2}>Actividad Ligera</MenuItem>
                <MenuItem value={3}>Actividad Moderada</MenuItem>
                <MenuItem value={4}>Actividad Intensa</MenuItem>
                <MenuItem value={5}>Actividad Muy Intensa</MenuItem>
              </Select>
            </Box>

            <Box>
              <FormLabel id="demo-radio-buttons-group-label">Sexo</FormLabel>
              <RadioGroup
                row
                aria-labelledby="demo-radio-buttons-group-label"
                defaultValue="female"
                name="radio-buttons-group"
                onChange={(e) => setSexo(e.target.value)}
              >
                <FormControlLabel
                  value="mujer"
                  control={<Radio />}
                  label="Mujer"
                />
                <FormControlLabel
                  value="hombre"
                  control={<Radio />}
                  label="Hombre"
                />
              </RadioGroup>
            </Box>
            <Box fullWidth style={boxStyle}>
              <InputLabel id="nivel">Objetivo </InputLabel>
              <Select
                labelId="objetivo"
                defaultValue={0}
                fullWidth
                id="objetivo"
                label="Objetivo"
                onChange={(e) => setObjetivo(e.target.value)}
              >
                <MenuItem value={0}>Ganar peso</MenuItem>
                <MenuItem value={1}>Perder peso</MenuItem>

              </Select>
            </Box>
            <Button
              type="submit"
              color="primary"
              variant="contained"
              style={btnStyle}
              fullWidth
            >
              Calcular
            </Button>
          </form>

        </Paper>
      </Grid>
      
      <Grid item style={gridItem}>
        {calories ? (
          <TextCalories imc={imc} kcal={calories} obj={objetivo} />
        ) : <></>
        }
      </Grid>
      
    </div>
    </>
  );
};

export default Calculator;
