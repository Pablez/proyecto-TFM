import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { Register, Landing, Error, ProtectedRoute, SharedLayout, Profile, RecoverPassword, Contact, FaqPage, Home, ExerciseDetails } from './pages'
import { Calculator } from './components'

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path='/'
          element={
            <ProtectedRoute>
              <SharedLayout />
            </ProtectedRoute>
          }
        >
          <Route index element={<Home />} />
          <Route path={"/rutina/:exerciseId"} element={<ExerciseDetails />} />
          <Route path='calculadora' element={<Calculator />} />
          <Route path='perfil' element={<Profile />} />
          <Route path='contacto' element={<Contact />} />
          <Route path='preguntas' element={<FaqPage/>} />
        </Route>
        <Route path='/register' element={<Register />} />
        <Route path='/landing' element={<Landing />} />
        <Route path='/recover' element={<RecoverPassword />} />
        <Route path='*' element={<Error />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
