export const exercises = [{
  "id": 1,
  "nombre": "Rutina de pectoral",
  "duracion": "12 minutos",
  "grupoMuscular": ["Pectoral mayor", "Pectoral menor", "Tríceps","Deltoides"],
  "dificultad": "Intermedia",
  "descripcion": "Rutina con mancuernas para desarrollar el pectoral en casa",
  "material": "Sí",
  "imagen": "https://www.cambiatufisico.com/wp-content/uploads/entrenamiento-pecho-696x392.jpg",
  "video": "https://youtu.be/LBDqQA8AVD4"
},
{
    "id": 2,
  "nombre": "Rutina de espalda",
  "duracion": "9 minutos",
  "grupoMuscular": ["Dorsal Ancho", "Bíceps braquial", "Redondo mayor", "Trapecio"],
  "dificultad": "Intermedia",
  "descripcion": "Rutina express con mancuernas para trabajar toda la zona de la espalda",
  "material": "Sí",
  "imagen": "https://fitnetizate.com/wp-content/uploads/ejercicios-espalda-gym-primera.jpg",
  "video": "https://youtu.be/42Ufgq1oeq4"
},
{
    "id": 3,
  "nombre": "Rutina de brazos",
  "duracion": "25 minutos",
  "grupoMuscular":  ["Bíceps braquial", "Tríceps"],
  "dificultad": "Intermedia",
  "descripcion": "Rutina con mancuernas para desarrollar el pectoral en casa",
  "material": "Sí",
  "imagen": "https://media.revistagq.com/photos/5d8b2360e90a3a0008bddad1/master/pass/tipo%20de%20pesas%20para%20crear%20mas%20musculo.jpg",
  "video": "https://youtu.be/hSvt5dVeg1Q"
},
{
    "id": 4,
  "nombre": "Rutina de piernas",
  "duracion": "50 minutos",
  "grupoMuscular":  ["Cuádriceps", "Femoral", "Glúteos"],
  "dificultad": "Intermedia",
  "descripcion": "Rutina con mancuernas para desarrollar el pectoral en casa",
  "material": "Sí",
  "imagen": "https://t1.uc.ltmcdn.com/es/posts/9/8/7/rutina_de_ejercicios_para_definir_piernas_44789_orig.jpg",
  "video": ""
},
{
    "id": 5,
  "nombre": "Rutina Fullbody",
  "duracion": "50 minutos",
  "grupoMuscular":  ["Cuerpo completo"],
  "dificultad": "Intermedia",
  "descripcion": "Rutina con mancuernas para desarrollar el pectoral en casa",
  "material": "Sí",
  "imagen": "https://p0.piqsels.com/preview/652/66/526/fitness-women-sports-teamwork-thumbnail.jpg",
  "video": "https://youtu.be/L39nY2kGBow"
},
{
    "id": 6,
  "nombre": "Rutina de cardio",
  "duracion": "45 minutos",
  "grupoMuscular":  ["Cuerpo completo"],
  "dificultad": "Intermedia",
  "descripcion": "Rutina con mancuernas para desarrollar el pectoral en casa",
  "material": "Sí",
  "imagen": "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/treadmill-testing-0367-1578930314.jpg",
  "video": "https://youtu.be/VKOYzh10ipE"
},
{
    "id": 7,
  "nombre": "Rutina de cardio intensa",
  "duracion": "30 minutos",
  "grupoMuscular":  ["Cuerpo completo"],
  "dificultad": "Intermedia",
  "descripcion": "Rutina con mancuernas para desarrollar el pectoral en casa",
  "material": "Sí",
  "imagen": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUWFRgWFRYYGRgaGBgZGhkcGhwaGBoaHBgaGhocGhwcIS4lHCErHxgYJjgmLC8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QHxISHzYrJSs0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIALcBEwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAFAgMEBgcAAQj/xABEEAACAAMFBQUFBAgFBAMAAAABAgADEQQFEiExBkFRYYEiMnGRoRNyscHRQlJi8AcUI4KSorLhFTND0vEWU5PCJIOz/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAIDAQQF/8QAJhEAAgICAgICAgIDAAAAAAAAAAECERIhAzFBUSJhBBNxgQUUMv/aAAwDAQACEQMRAD8AotqsMktSS5bWgKspy8REQWVq0z841P8ASFZJa2lSqKpEgnsgLnV+GuUUZUGKGoVSGLBdyM1HnrL95qfGD3/T8nB2LbLJ1pjTUV58zBzYq5bPaJk/28sPREK1JFM6GlCOUHn2KsJJHsNM++4y8MWkFJeAtsye87nJqCyvTRgQacqrAGZdbqe7UcqH8+kaPbrrlyfbhBQCcqAa5ATKZnPdAN5cEo1tGxlZTSCDQ5cjHoMWW1In2gD4xDlXYjnKoFdx184xNmtAgNC1MWGZs0hNEdgedCK+kQp2z85ASMLKN9aehhxNA8GFAwhUbh6wuUCxCqMROiggt5DOBSj7McWT7qtiy3xsuIYWUgUqKjIrUUqDT1iz2baOy0BdJobeB7Mrqd+CpypwiqLYJx/0Zh8EY/AQr9RnDWRNH/1v9ISUeObtsKkukSb1tyTGBSWEAZzUYAWBIpUIigaE0z7xzghcU7BapJH2iiMOIcBTXqa9IES7JNrlJmnkJb/SLfslcU32gnz0KKlSitk7tSgOHUAVrnvpDynCMHH6Fjxycky237eSyJWOlWJCIu4sdOgoT0gNdFkepdmDO5xMSRXPpD9+S8c2Ws5SJWGvtBokwnJTwAWmf4jBCTZCjBXowpVWG8ciNY8p9nd4CUqSeJHHSA22dkJszMAxMvt4R9qm7yJiwWZIXb5OKWwGpU08aRWIt0zBJt4MRXCanPQ5fWDd1zAZKAkDv4q0H2jAm22dpbsjaj4bvzyiMTHT+iMlaZvF+a+KV1Zb0KfeX+IfWAm1FgyWYN2R8DofP4wMUCH7dbHSUEVgUYCqkA0rUZcNKxv6HHaZ1cn+TjzRcJxq+mn5AxWEFYeIhJWMPPGisJIh4iEEQANUjwiHKR4RAA0RHhEOEQkiGAbIjyFkQkwAeUjo6OgA2vbGXbKrMtSSgxR1Hs2JyAOoOY72sUxHjVP0j1Mpa/dm5fuChBrGToIdsnHtly2MtFpQzJlns4migViXVcNO0KYmFTlFha+rfXEbBkd4mp8cUCtgrQUkzaAklwMq/dMH7NbCzUKsM6CqkAmByrwbiVC+pr0mGYhR2nIxUkGlUmHUHPWA6S8WkWrbZewTv9rJ/wDym5ekVmzT0U9tlWgxEsaClafGnnA5WkZFdlYvewNLmMHJJdaoFBIXExpU01grdUkIoGtN/E8fOJF/gPNR1JKiWFruLVNacaD4iPbKmggjbNk9BGzJ6xD2stWCWsoEB318P+PjBewoNToBU9Ir99SHmyxacalTMZVQL2wASlS2uHsk05iG5JYxJwVyKzJao8vKucP7Pr/8yp3M59D9YZsUSNn5f/ynoe6HPPhHNN1F/wAHTFfJGnXK+MsBXIVFMjWDNnctk1Qd9R9Ir9z2gSyWcnTKg+NIsNktSTBlMWgzOI4SPOOFSOhokVA+Q3wm0WlJKPNmHsopY8gBWg5/WClmsSFcasGqNdYoe308u0myCn7R8TgkiqJnQU8+kPG20TfRebNJSdLzAIYVPOuhgY1gmWfsqMcqvcP2fdO6GdnL+kpZ0xMo1Azp2VYqD1oIJ/8AVNkZsDOoJ5iGcU19hZ7ZJqniOTZH+8TXTKPJSI4qjKy+IMOfq4Gh6VyjUmY6Mc/SVZcFpx0GFl+BB+LmKX+tLwi//pmYq0haZMrmvHCVy/mjN8OS+Ajr45SUURlGLdkoWpeEN2m0lyABlEciJlklZVikptIVQjdnYYQVh9hCCI50xxgrCWEPkQ2whrAZIhJEOER4RGgNUhJEOEQkiABsiEkQ4RCSIYwbjoXSOgA+htvrNSSWxM1RMAUgDABLOQpGYy5RjV9v3V7MMBKkM9ezhaglTCwoy6GgzpGMWy2z1lS2Qkl1qSEBpRiKCgyyEO70JGrZqv6M6CVOBJH7RPtUy37+EXoNKoO0OrE/ExgNwPbWls7YfYg4ZgZFxLiXJhjQ0GmYh+1SZ+JjL9myDTGAXIwgmuFaVrWFk2ju4OCHJHK3rVIvO34GFiuYxyM61zwT9/lAewbMJPlK7TXXGvdUAEAMRqSRqK6RTxe9uYJKaUySvaJU+zoFPaFcRUCtGaD9gvy0JJIqcK40lnCBXtuq50zNanoYoqffo4Zpxbxfn+yHfLgziiu7pL7Cs5xMaHtGoAGZr0Ah6xpA+zpBmxy8xXQZnwhoxS6JybEX5a/ZSMINHmZDiBx/PKKwL4tP/fm/+R/rBTbNCGksf9RGcDguKi+YAPWK2DHLzPKRbjVRCH+MWj/vzf8AyP8AWFWe8p7OKzphGpBdiDTiCc4G1h2yTMLqTpWh8DlEWkVi9qzQrqvd8C+0ko43EHCeoIIPpByZelmTsvJdDxCqw6EGvpAbZpMRCNqv1iy33ZF/V3YLVkUsONVzp6RLBPwdUkkJse0NlCO6uwRHMs9l++oqQBTPKmemcVS/LYk13nozftEVEByolSSxHEg5cAeeQtpbLYpCDvzA0w82muaE/uInnHOO6i6KAteQ1MVfEo1RzKTd2Is1RppoBE2/bvk4UcsAxYUHEaH5R1mlVPIQBRGt9rCISEGQI+zLXvMOZ3c2URZwWOycZtS0XOwSUQKV9CYu13uHSnKMwmWmUZ7rJJVVICgHsnCADTjprv1i23TeVKUPIiIJ4umdtKUckCf0lXG8yQpQFmlsWA3kMAGA8gekZXg7IyoafCPo44XHGsZ3ttsgambKHa3qNG/vF4yrRzzhe0ZjhgmiUUCIyJRqHI10gi6xvK6okiK6w2Vh9hCGETTAYZYQwh9hDTCGQDBWEERIIhthDIwZIhJEOERxEADJEJIh0iEkQwDdI6FUjoANwvi8509Jod1aWEmMg9hNlMDgcZl+y2R3HnGf3cMUmUMRFEbQlTmRWpGZjSb5ttmaQ4R0DFX7K4iO4wG7LdGaXcCbPKK1VqMCcjkCBoRln8Io25JUqIQ03b9Fu2ZsytJmI7TSjzJSsFLtUY0U1JDU74/hizydm5BBA9sf2jJ2nfNVIDVoVqasc+UVHZ68lkyXDzmDF8QTCueFGYEkDLtKnCLQdppDMhSeUq81mXtUozlhpQiuR6wsnLpWN5Au2F2rIVkRnwq8o0LuR20mEGjORqh8zFSSaxlohrRS705u7MP5SD+8YuG0l5Webj7Zevs2Oq5S1mAgMS1P8wAZZ16xUpQLGp1JJPiYpvV+hVW/5JFmlwYs1nxYU++e1yRc289OsRLJJqQIelW4B2I90e6PqfgIaTxiZ2wb+kR6zJXJGH80U+sWTbOdiaUfwv8A1CK3HFLbZ0xXxPawqUe0vvD4w1Hss9pfeHxEYMjWNlEq5Yxc7TJqjDiD6iKVsrM7R/O+L5iGDpE49HXIzC9iqu2HuoFky/dRQhb+WnVohWaWdd5h69DjnuNwYjwA/NesTLFIrTyEdMI5O2cE5KKpDtnuz2qtL0xKa0bCaaZHCaVrw4xHtN3Jd9mmulQ7gIvbx0qSBQ4V/ET4LAy6dpQlsmBzRGyYtl7MrQU5jEKfvV4w7+ky3dqWgP3m8qSx/ST1hfk+Rt9Lo34qKXllSkuQag5xYrpvIg5mKelsUa+kE5VpTJsXTU+QhZxtD8fI4v6Nbua8agZxZsAmJnGRXHeumZ6xpez94hhQxKLp0zqkk1kjP9vdmghM5BSnfA3j73SKlLeo5xu9/wBiWZLYEVqCOhEYWJIlTykxaqrYSDw3HyjpSzVeUc3Jr5DLiG6RabZdFmZayyUbcQar1B+UV9bHMLlFXEw4HLx4wji49k1JPoiMsNOIsSbKWoiuFRyJA+JiBeFyWiUKvLOH7y9pepGkbjL0FoEEQlhDpEcJLEFgpKjVgDQeJ3QARiI8IhxhCGEaAgiEGFmEmGARHR7HQAaU+y96KKmVZVHGrE+pMDZlxW0GrCzV90mNvvlv2cUO1E1h312LHuqKlYNl7a9SrSBTUGUCPIiJczYm3b58vwEpf9sXnZ0ZN0gnb56orO2SqCx8AKmEVUO4mR2q6plmYpMmB3YKxooUKu4Zbyc+gh6zpHlotDTZjzG1di3hwHQUHSJVml1MWiiE2SFBCUGTP2QeA3noI9s9wHLt18IsezNixYppGXcTwB7Z6tl+7BabdEpjULhNa1SqmvOmR6xOfIroePG3GzKttbL7NpK/hf8AqWKuTGlbX7Ol3SsxiFDUyUHMjXLPSK+NlE+8x6r9I4uT8iEZOzpjwyaRUyY8R+0PEfGLa2zEscfM/KPU2YTh6tEn+Xx/Y64JBvZSf2hF/n2nDLJO4RnNwyyj4RuMW6+5pFnPh8opB30XktFJSbid2+8xPrHl43yJcyTKUirOC54LmF8M868FiNYJgUM7aAEmK9apBmuXqQxNeQ3AeWUd7ahGvZ5aTlJvwgnfdoRZzzEly5iM/tEfOqnJiuTDIEHxzgVf17m0zFmNRGCBaAmg1qRXMVrWJkm5WZGxzDhVSwVRSpFK1J/CWgVaLOiMQSSPu1z8cwYV1WvIyVPZHmy1C1Q6Url84YEwHefARK9onayIXhXOPXw0OBn5BgvxEFG2P3basGlQAd8aXstey5Zxnk67nEtHUh1bPIhm5gqM1I/PEzLpmuhGFXrUVqpwjnWI8sV35OzhjOvo+gLNMDp4iMg/SLdRlzhMAybI+O6LpsheuICp5RO2zugT5LZZ0qDz3RkJbTFnDtGSWCe7lZa94mgJ0A3niaCLrd1nlyhRBrqxzZubH8iM+u+0mTOGMUKkq3hWhi8LO0odRWOppXZxy9BJrVES0WowOmWomYspAC7Kz5sFGFRU5nfrlyhm0e1TvyZi8wuNfGqYqdaRTdEtXQG2hu1VBmoABXtqMhnvA3cxA67pU51aWi5PmThYtwyoabt8EkR7XOEtWIRc2Pz8dw6mLtYbIkhMCKBxO88yd8ScFJlcnFFRs+xb0q7BfeYk+Sj5wp9jF3TFrzDgfP4Ra586mpiK00RRQiTzZRrw2XnSwWUB1G9Tip4jUdRAB1pkco1MzaaGAd93Uk4FlAWZxGQfx589++FlxrwNGfso1I6FTOySCuY1jolRU+mr7mEJFIntUxbL6fsaj4xT310hZy0NBB65GoDpArbq8iEWSDm5q3uKfm1PIwSugUBrl0ih3rbvbz3f7NcKe4uQ89esHHtm8jpDUlYLWWUxoq95yEXxO/wAz6RAsyVMWfZ6zguXOksYF98989BQdTHROWEGzmjHKSRZrLKCIqKOyqhR03nnDxfnDAePS/MdNY8tzPQxBt80JEBnXgRBW8iDv+UCnlHd8z8DHHy7kXh0JXxEcT+IekJaXy9IQ6gAsykACpOWn5yiKhk6Q7lStjFhoJwG/XpB2/8AOTQcIq11MWnFzvNfDgItl6D9n0j1YRxaXok3lCzNLW9FCDxb5CGsQRCzZAanrCZynG1eJr5xMmCWy4GRcOHM1apHEmtAfAbo6W8nZwJYqh+7rUrCqkMpDKaHcykHw1hFouKU+hFfxZH+IQJfZwg4pEwqeBJB6MvzEKW22yTlMl41H2qf+y1HnD1QraPLZsu6js1p/EOjCBjXTNXIAHgK0Prr0iy2HaxK0o6HhTEPSCku9LJNyfsH72Gg6g5HpSNSb6MuijWW1T5DB0Do2oYVH9iIs/8A1deBlYlnLQAYqImMA7z2fhC7bgl9x5cxDpTOg8NR4GAlmm0dq90kgjcQdRBDJupI2c6jcX/RYNkr6LPUkli1WrvJ1MbFY5wmS6HhGE2JllTlCkGprnrTIhviPEGNf2fdigIjnnFxkdfHNSgmVW/NkWm2giUikv3mNRgH3qj4R7fljk2YS1E5XcLhmDKvvZaeEXSZO9tN/V5b5CjTipzA3JUZ1b0FeUZl+kCRL/WWWzSwMJIdxkC+9Ru7OVedeEdPDC1bIfk82lFJa8+SBb7ZJLpR2VxXA6Eqwrlkwh43/Plrm6uFBGJwRM5dtSA1K7xAVbFp7SYgANRSpMe2u0SMDAuzmm7KKtOPTOVSUu0W7ZGyYLOrnvzO2xOueg8oKWmbSgGpyH1PKGrsI9hKp9xPgIS+czwTLq2f9IhY9DMbw0z1PHf/AGHKEFiNCY8t0wojsoqQMoI3Jca2izpNWbODkNjaiugYOVw4KZZYd4yzyiqWiTlToCzW5fI+YhnF4wYtGz9oFcDypgG7F7N+OYNVHUwCtLmW5SajIw1BzHLMajnGNM1SRHn3bKZix1OsdE/DHQpS2X+9n7NKnyMVvASYLtJEwVaciA7gcbeQy9Yi2oWaWOwGmPxY0H8I+sckot/R0xkkD77t3sbMyg9uZ2B4fbPll1EVKzrDt7WozJpzqqdkDIAfepQccukLskupivDCkR5ZWyfJ7CFzuGXidIs11uwlqsqVMcAZthoGY5sQcwaknfFfwkuqqxXBRiRSuI6DMbh8RBizS3IoXcg6hpjkHoTSIflTTePopwRaWQ/NvVkcI4wMdAQGPUKSR1pE5Jjt9sjwA+kNWaxBRoB4CkTVVRzjif0dFsF26yTmzVy3IPgPngNeggS8og0c2hDxBDr/AC9odVEHLdfEiV35iLyLDF0UZxUL62yRu4juo3lcIHPtZ+kUjFy8CudeSY01kzWbiG4lVIPUCIN43yuHCzIudeGLgNYEyrZPtRZA6opANR221p3mzEdbdk56p2VltXV+0HPixZq+kdUPx4pqVbIy5nTVha5bSjHEGHmKxYL0vGWssMZidmlVLCpBFNOsZSuz9oLYVlsx4jTq2VIul17OWWzoHtLylelWxOXY8giHPwoYr+jJth/s4xoE2uWpcsjY0fNWFaEdd40MeyUYCmRHA/WHto77kzMCWc4ETTFhRSeKpp84Epapw0ow8AfVYyMMdN2K5Zbqg5KcA5hlzBJ1GlNRu8Yn2NTqCGFANcyd5r5ZRWhfTrqinzH1h1L/ABWvsxXiGKt5gRXRJph68LrSb3hgb71AD/EMohjZE7pmXhD0jaaUNWccnSv8yfQwRs992Vv9QITzKj1HxEBqKnfd1NJSoevT5xNuufIkgNZ5LWibTFjmrRENKnCm8jiYtgly5n2kccAwr6VB9InJYkCOqIAWXCAMjVsgMjSBZA8SmLdkyawnzfZ1J9pkAKVzIAWlPCJd7bbzJaewsy4CRRph18EG7xMFrTck+XLpgyA0B/tFTsdx2i0zTgTAoNDMYGg90faP5yiEY8sp3Na8FnPjjCoPYc2XvE2GzvOmNSdaD+zU5sEAzmv1JoN5p0GWCyva5tcwgPpWpxHeTmT4xLtOwTgF3tIAAzJV2NB1z8IK3JSSgSWCScgado/iPOOlyxVHOo5PJgnaW4kVKSwAwyyrrFJWwurlXH2SfED+8aZb7DNY1ZGI8TT0in2yVSe4IphVVpnvz3xzwzc6a0Xm4KFxasseyVqx2ZVr2pdUbp3fMUghagQQ4zw1DAa4TSpHgQD5xR7rvA2abizKNk45bmHMRf5U5XUOjBlIqCNDF1rRHvYzUEbiCPEEGIJsCKcSF0bcUYikS3s9KlGw13UqtfDd0pDLO41UH3TQ+TfWHTaJyin2T/8AH7SO/wCzm82BR/Gq9mvPDA6+L4mzpbSvYYcVKN7QuAA4atCKYqjvCmRIpDD2ld9V94U9dPWGZ1pVRiJFNa7obKjMEx6QmFVXFoAPSPIrU2/qk0YAbs49ieaK/rZAS1P94+cTbLekwHCD55wJBidYE+1x+Ec9JnRYRs6QYslEUsdAKwOsyVhN/l/Z4Jaks2bU1C/30846I6RzydsnWbad8GBbPvLFmZQSxPe1qNw8AIJSNp3CCklWfh7QKPURnYtM6XkwYD8a5eZHwMSZV8j7SD91ivxDRzvii3bRZSaWi1W7aa3n7Cyx+FC5/iOXpAG03rPf/MmzG5FiB/CtB6RMs+0CHWY499Aw8KoanxpBA25JgoHksa5EkBjywTADGYJdIMvZV1cDQAeEJebFmm3UpzaVQcVDCv8ACSvpEG0XOlaBnU/dID/7YU0CWO2LLfErYDoRqp+kW+xbaLgKOAa7w2/jnFbtGz5rkyP5qfUU9YHzboI+yelCPSLRk6JyjF9h297UJ9cBK1z75GfHLUGBdnul1OJnQc9TA02IDeR6RwkMNG9YZybBRSClooD3wx8MoQk1K9qVLboQfMGIQZxzhQncRCNWMtBET5X3HX3ZrEeTZQ3MmfdZjydEb1ABiIHXjCwYKaDQ57c70RvDEvwJjv1hd8th7rq3owEJBhQMbszQkzEO9x4pX+kmJlltZHctIXljdPDJqCIpQQ2UhlJoVxTLJYrRa5jqpnOyVFaTFYnwzNY1y46JLq7EADNnC16toIwKz2cswwqK7iAMusXu65CS0BnzGcjMKzEoDyBMPnaExp6LZtNeqzFwo7FN57qN4b2Hp4xTDtI1mess0cilaA0HAQ1fl/1yU+GXyioPaHYktgPQj6wiVvZRvWjQl2/nkf56A00ZG18aUis2q+1mT2mWjE4JQHAAKoqjIEAgZ1zMAPbHevkwPxpHhnLvDDxWvwrFFS6ZN21TROmWlGGjU9xvpCLuvlrMTgc0OiEHCTUa1zGVcxERbQBo1OuH40hxpzNqQ3jRvU1gab9GJpezSZruO8hPNDiHkaH0MVzaC+SgUJUEtnVSKADOlR4QLO0tq0Myv7q1+EDLbaTNbE9SeIwiuQGdFz0EDT8AmvIVF6ziKih6D5QJtyzHJY1z3AUHkIkybSFUChyj1rUvAw9JiJtAb2Z4Hyj2DXs3/wC2/wDCY6EuPtFfn6ByCpA4wZkLpDFpu0yXFTiBGRoQNdN+cSZBiEN7LT1oKWWgFToBU9I8kPiqx1Y18BuHQetYi2mcKBBvNT4D6mkcirwpzFQfSG5JVoXjjewqhhLXZKfvS0POlD5rQxFStVoxIBzqd1DwpXrWJNmnuMOIgVYjMbq9kZHUjfxBiSZRojztkpDd0ungcQ8mz9YHztknHcmq3JgU+GIRZktfLKjmtdMJpEiRMDKGGhFRD5E6KL/hFslElEcfiltmf4DX0j1dobVLOF2PNXRTXxxLi9Y0ACPHlBhRgCOBAI8jG2n2gplIXaioo8iWRWvZxJ50rElL8szd5JieBVlHTU+cHbRs/Zn1lKDxSqeimnpAu07HSz3JjryYBx6UMbURdg61NKfNJobkwKH+HOvnEWZZRSoKnpQ/T1h+fslPXuMjj3ip8my9YHzrstMvvS3HMAkea1EOkjLEugGoI8DDTIp3+YjwWlt8KFoGhHxEGKDJiGs3AqetPjDZkuNx6ZxIxLujsA3E18YMAzIyzWEOraIWS2818c/jCSRvUeOnwjMGbkhazlhwEbiIjkLwIj0S+BgxYZInS7XMTuKg551hqba5zd5oZGIQ4uM7oEkjG7R4K74VSFNKcZlGpxoSPOGvaCG0Ls9KQ2UhzEI4mCkFsZZIaaSN4HlEkmEGFaGTI/s+BI6mPVVhox9DDyoTpBGzWHe0NFNiuSXYPwNSrAEcRl6QnKlfyYl3laRTCNN0Q7OdAfLrDfQu+y32RlZFatKgZVJpHQ7dllf2SdkZiuvE1jo43R2IrV92uYUNCRmK0yy6dICLa5i6O3xp5xYbcgwkAAdQfhAsywcvgDGxlQONkI2+bXEWNeYFPKJki/GHeUHmMoWlnypQ05iGnu4HRSPCNck+zEmugvZr7lnVivvD5jKDVltasKqwI5GsUc3eeMeCzMpqpIPEGh9IzRuzRldT5EdDrEqTMAAHCM7kXnaE+1iHBhX119YJ2fammUxCOakH0MAF8SZDgeKzY7/kNo4B4N2T6wYl2objG2ZRPrHhMR1nQ8JghkxGjqR6FjzFHuOGsyhM2yo/fRH95QfUisDrRsvZn0RkP4GI9GqIKK0OB41SFcSq2jYofYndHT/2U/KBlo2VtKd1VcfgcE+TUMX6sew6kI4sy20WWanfR195SB56Q0kzlWNZVjEefdsh+/JQnjhCnzWhhlIVxMwDLvELCruMXydsdZm7pdPBgw8mFfWBtp2Ff/TnI3J1KHzFRDZIWqKuic4mywp30PH/AIh+07LWtP8ASZhxQhx6GvpAyZLdDR1ZTwYFT6xtoAiltmJowPM5+ph43+SKPKR+ZFf6qj0gOJxjsY3xjjFm5MKNabI47UnAeKk/+pUekQp1mkHuOw8SG+Q+MRyFMKSzZV1hf1rwb+z2Rp0oqcjUeFITKqxpEx6whWNezrBgGYSstkwjE0M2+2ACg6CG2tMxgMTDlWg/OkM2ez+1YBQWzoDQ1cngNy8BqfSGfpCJL/qRHsshnOJoJPLV6HRwVHvVNM+cTb2ux7MQrrSoqKEGvHQ6iJOy1lxP7VlqErTmx+gz6iEnJRiNBOU0WBJ8lQFDMcIA7nAU4x0SDam+5/MfpHR5+j0ipT2TTCo51ziI6puPkIlJKTmeh/4hxCi6KPSFbCgSw4BvKPfZOfsHqYMm0im7pnDRO8E+kapBQPSyudy9BX4wo3Ux1JHkoicXc5BW9aQn9VmHdTyhrCgbNuhRvDHhWp+kR3ukj7NB5RYFuw6lj+esRp0twxAzHGsapGYldnXd4fnwhpA6dx3XkCQPKLEbMW1hD3WvGNUhcQfZ9o56d7C45ijeY+kFbLtbLPfVl594emfpEGZdyjeD4ZxBm2QcIZSMxLvZr4lP3HVjwrn5HOJqz6xmTWFjmFPiAfjDki1WiX3XYDgTUeRrDWhaNOWZDivFAs+1E1e+itzUlT61EF7NtVIbJiyH8Qy8xURoUWoPHoaBdnvBHFVdWHIg/CJSzY1MVxJgeHUeISvDyPDKQriCr02idJnsZCB3GpbStK0AqK5c4Qm1kxP86zsOJFR5BhT1iu3k7SbU7lcXbLgHRlcHLyJHSDRs7qS0tT7MqGUozUzbOiqSKBc6YTWlIeUqrQkY3ew7Y9rbM1MTMh/Ep+K1EGpF4SZooro44VV/Q1pFCdVJAdASd7y6blJONcBAGLetRQ1hhbBIbOrJphwYnBrwUoGPOlaU1gXJF/QPjkvTL1adnrK/ekIDxSqH+XL0gPathZDdyY6cmAcfIxX5rWuzn9nMmslAVYYitDxU1CnlBa4tqXZxKtFKk0V6YTi3Bxpnxy+cP2rTJ0k6aoH2vYOeO48t+pRvJvrFcCFMauzB0OFVADKWD0arVyAAOlamkbGHjGbZMq7ni7nzYmNTBpj132xg4qFYD7LVAPjrEu878DrhElEpvGfyEDbNOo4/PqIJWlJYUFUQHeRUn1Jhk3XZJpZJUDlVnzbIcN5gpZpuChQlWUggjIg7jA93pDs14xS7KSh0TrxvCbapih2qclXQKBvOVAAM4s8gS0lqiPkBurmd5NOMBbpuwgYm7x9BwgolkLZKCfAVMcfLPJ0ujs4eNRR7hT7x8v7x0P8A6g/3f5lHxMexGi1oqQlsdTC0kCtKkmOjoU0JSbtandA5k/SJUmWg3j91fm1I8jowCQXUDMHqfkohibeCDeByCk/1R0dAgBcy1gtVQaV3w/KLtQBQfKvqY6OjQJ6XJNObMF8T/tBjwXNL1LlvBaerH5R0dABz2SSn2SfeYn0UAQ2zKBkijwVR65mPY6Ms2kRJ8uuo9awOn2AcY9joZMxpA6fd/L1iE9g5COjooibE/wCFzFzHZO4hqfCH5V52mV9vEODZ+usex0MmKErLteR/mS+qH5N9YOWC/wCVNyRjXgVIP0jo6GM8j942JLQoxVVh3XGo5EbxAV9n7QmcuYGA0wsyN5HIecex0UTZNpHJe7y/2doV2Kk0ImdoA7m1V+sPyLXJYgLMKtSgDpRhTWhl9gcO7nHR0ZKCNi2SRKwYA00o7EFKYnUqq4AGyFSS2I5DPwrAzaKSVmIpoWElAzDLERUYjpnkPIR5HRvF2jOTpl5u+2M8qW51ZEY+JUExk0x6knmY6OhyZ5KbOJzTCaCOjoZdGNfJHmMKKUqx37gOXOC1y2VCfazTp3VAqfE7o6Ojnm9HRFKw41uAyC+eceLapr5AkjhWg8tI8jogWFewflHR0dGAf//Z",
  "video": "https://youtu.be/VRRthRzvrvI"
},
{
    "id": 8,
  "nombre": "Rutina de calistenia",
  "duracion": "10 minutos",
  "grupoMuscular":  ["Cuerpo completo"],
  "dificultad": "Intermedia",
  "descripcion": "Rutina con mancuernas para desarrollar el pectoral en casa",
  "material": "Sí",
  "imagen": "https://cdn2.picryl.com/photo/2012/11/28/no-problem-corps-to-implement-pull-ups-for-females-1f2f15-1024.jpg",
  "video": "https://youtu.be/UqB65gs_Lr0"
},
{
    "id": 9,
  "nombre": "Rutina de abdominales",
  "duracion": "45 minutos",
  "grupoMuscular": ["Recto abdominal", "Oblicuos", "Core"],
  "dificultad": "Intermedia",
  "descripcion": "Rutina con mancuernas para desarrollar el pectoral en casa",
  "material": "Sí",
  "imagen": "https://media.revistagq.com/photos/626a7c8fd3f59913a95c5021/1:1/w_1600%2Cc_limit/GettyImages-590575137.jpg",
  "video": "https://youtu.be/zjrH2fLnh1I"
},
{
    "id": 10,
  "nombre": "Rutina de espalda con mancuernas",
  "duracion": "45 minutos",
  "grupoMuscular":  ["Dorsal Ancho", "Bíceps", "Lumbares"],
  "dificultad": "Intermedia",
  "descripcion": "Rutina con mancuernas para desarrollar el pectoral en casa",
  "material": "Sí",
  "imagen": "",
  "video": "https://youtu.be/KHUvqjktkNs"
},
{
    "id": 11,
  "nombre": "Rutina de cardio",
  "duracion": "45 minutos",
  "grupoMuscular":  ["Dorsal Ancho", "Bíceps", "Lumbares"],
  "dificultad": "Intermedia",
  "descripcion": "Rutina con mancuernas para desarrollar el pectoral en casa",
  "material": "Sí",
  "imagen": "https://p0.piqsels.com/preview/950/344/886/fitness-women-sports-gym-thumbnail.jpg",
  "video": "https://youtu.be/ksi-c8YgClY"
},
{
    "id": 12,
  "nombre": "Rutina de piernas para corredores",
  "duracion": "24 minutos",
  "grupoMuscular":  ["Cuádriceps", "Femoral", "Isquiotibiales"],
  "dificultad": "Avanzada",
  "descripcion": "Rutina sin material para mejorar la condición de los runners",
  "material": "Sí",
  "imagen": "https://www.telemundo.com/sites/nbcutelemundo/files/sumo-squat-sentadilla-sumo-gettyimages_0.jpg",
  "video": "https://youtu.be/_-ACPPCj5E8"
},
{
    "id": 13,
  "nombre": "Rutina HIIT",
  "duracion": "7 minutos",
  "grupoMuscular":  ["Cuerpo completo"],
  "dificultad": "Intermedia",
  "descripcion": "Entrenamiento interválico de alta intensidad para realizar en casa",
  "material": "Sí",
  "imagen": "",
  "video": "https://youtu.be/0PET7imkXWU"
}
]