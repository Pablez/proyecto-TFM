import { FaWeight, FaDumbbell, FaEnvelope } from 'react-icons/fa'
import { FaQuestion } from 'react-icons/fa/index.esm'
import { ImProfile } from 'react-icons/im'

const links = [
  { id: 1, text: 'Rutinas', path: '/', icon: <FaDumbbell /> },
  { id: 2, text: 'Calculadora IMC', path: 'calculadora', icon: <FaWeight /> },
  { id: 3, text: 'Perfil', path: 'perfil', icon: <ImProfile /> },
  { id: 4, text: 'Contacto', path: 'contacto', icon: <FaEnvelope /> },
  { id: 5, text: 'FAQs', path: 'preguntas', icon: <FaQuestion /> },
  
]

export default links
