

export const BaremoActividad = [0, 1.2, 1.375, 1.55, 1.725, 1.9];

export const calculateIMC = (peso, altura ) => {
        return (peso / Math.pow(altura / 100, 2)).toFixed(2);
    }
    
export const calculateKcal = (edad, nivel , peso , sexo, altura) =>{
        if (sexo === "hombre"){
            return Math.round((66.47 + (13.75 * peso) + (5.003 * altura) - (6.75 * edad)) * BaremoActividad[nivel]);
        }else{
            return Math.round((655.09 + (9.56 * peso) + (1.84 * altura) - (4.67 * edad)) * BaremoActividad[nivel]);
        }
    }

export default { calculateIMC , calculateKcal }