import {
  SETUP_USER_SUCCESS,
  SETUP_USER_ERROR,
  DISPLAY_ALERT,
  CLEAR_ALERT,
  SETUP_USER_BEGIN,
  TOGGLE_SIDEBAR,
  LOGOUT_USER,
  UPDATE_USER_ERROR,
  HANDLE_CHANGE,
  UPDATE_USER_BEGIN,
  UPDATE_USER_SUCCESS,
  INCORRECT_PASSWORD,
  SEND_MESSAGE,
  SEND_MESSAGE_ERROR
} from './actions'

import { initialState } from './appContext'

const reducer = (state, action) => {
  if (action.type === INCORRECT_PASSWORD) {
    return {
      ...state,
      showAlert: true,
      alertType: 'danger',
      alertText: 'Las contraseñas no coinciden',
    }
  }

  if (action.type === SEND_MESSAGE) {
    return {
      ...state,
      showAlert: true,
      alertType: 'success',
      alertText: 'Correo enviado correctamente',
    }
  }

  if (action.type === SEND_MESSAGE_ERROR) {
    return {
      ...state,
      showAlert: true,
      alertType: 'danger',
      alertText: 'Error al enviar correo de contacto',
    }
  }

  if (action.type === DISPLAY_ALERT) {
    return {
      ...state,
      showAlert: true,
      alertType: 'danger',
      alertText: 'Por favor, completa todos los campos',
    }
  }
  if (action.type === CLEAR_ALERT) {
    return {
      ...state,
      showAlert: false,
      alertType: '',
      alertText: '',
    }
  }

  if (action.type === SETUP_USER_BEGIN) {
    return { ...state, isLoading: true }
  }
  if (action.type === SETUP_USER_SUCCESS) {
    return {
      ...state,
      isLoading: true,
      token: action.payload.token,
      user: action.payload.user,
      showAlert: true,
      alertType: 'success',
      alertText: action.payload.alertText,
    }
  }
  if (action.type === SETUP_USER_ERROR) {
    return {
      ...state,
      isLoading: false,
      showAlert: true,
      alertType: 'danger',
      alertText: action.payload.msg,
    }
  }
  if (action.type === TOGGLE_SIDEBAR) {
    return {
      ...state,
      showSidebar: !state.showSidebar,
    }
  }
  if (action.type === LOGOUT_USER) {
    return {
      ...initialState,
      user: null,
      token: null
    }
  }
  if (action.type === UPDATE_USER_BEGIN) {
    return { ...state, isLoading: true }
  }
  if (action.type === UPDATE_USER_SUCCESS) {
    return {
      ...state,
      isLoading: false,
      token: action.payload.token,
      user: action.payload.user,
      showAlert: true,
      alertType: 'success',
      alertText: 'Usuario actualizado',
    }
  }
  if (action.type === UPDATE_USER_ERROR) {
    return {
      ...state,
      isLoading: false,
      showAlert: true,
      alertType: 'danger',
      alertText: action.payload.msg,
    }
  }
  if (action.type === HANDLE_CHANGE) {
    return {
      ...state,
      page: 1,
      [action.payload.name]: action.payload.value,
    }
  }

  throw new Error(`no such action : ${action.type}`)
}

export default reducer
