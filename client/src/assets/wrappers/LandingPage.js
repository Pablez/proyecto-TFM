import styled from 'styled-components'

const Wrapper = styled.main`
  nav {
    width: 90vw;
    max-width: 1120px;
    margin: 0 auto;
    height: 6rem;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-bottom: 50px;
  }
  .info{
    text-align:center;
  }
  .info p{
    display:block;
    justify-content:center;
  }
  .line{
    background-color:#17034e;
    width:100%;
    margin-bottom:30px;
  }

  .link{
    text-align: center;
    text-decoration: none;
  }
  .page {
    min-height: calc(100vh - var(--nav-height));
    display: grid;
    align-items: center;
    margin-top: 3rem;
  }
  h1 {
    font-weight: 700;
    span {
      color: #17034e;
    }
  }
  p {
    color: #486581;
  }
  .main-img {
    display: none;
  }
  .carousel{
    border-radius:20px;
  }
  .main-img {
    display: block;
    border-radius: 20px;
    width:500px;
    height:500px;
    maxWidth: 500px;
  }

.containDown{
  display:flex
}

.containDown2{
  display:flex;
  margin-bottom:50px;
}

  @media (min-width: 992px) {
    .page {
      grid-template-columns: 1fr 1fr;
      column-gap: 3rem;
    }

    .fondo{
      padding: 0;
      background-size:cover;
      width:100%;
      height:600px;
      z-index:0;
    }
  }
`
export default Wrapper
