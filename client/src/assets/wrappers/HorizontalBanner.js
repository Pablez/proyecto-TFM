import styled from 'styled-components'

const Wrapper = styled.main`
  .flex-container {
    margin-top:40px;
    margin-bottom:40px;
    display:flex;
    align-items: center;
    justify-content:center
  }


  .item{
    display:grid;
    flex: 1;
    justify-content: center;
  }

  .item p{
      text-align:center;
  }

  .icon-container{
      text-align:center;
  }

  .icon{
      width:50px;
      height:50px;
      color:white;
      margin-bottom:10px;
  }  
  
`
export default Wrapper