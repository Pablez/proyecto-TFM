import { useState, useEffect } from 'react'
import { Logo, FormRow, Alert } from '../components'
import Wrapper from '../assets/wrappers/RegisterPage'
import { useAppContext } from '../context/appContext'
import { Link, useNavigate } from 'react-router-dom'

const initialState = {
  name: '',
  email: '',
  password: '',
  passwordRepeat: '',
  member: true,
}

const Register = () => {
  const navigate = useNavigate()
  const [values, setValues] = useState(initialState)
  const { user, isLoading, showAlert, displayAlert, setupUser, displayAlertPass } =
    useAppContext()

  const toggleMember = () => {
    setValues({ ...values, member: !values.member })
  }

  const handleChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value })
  }
  const onSubmit = (e) => {
    e.preventDefault()
    const { name, email, password, passwordRepeat, member } = values

    if (!email || !password || (!member && !name )) {
      displayAlert()
      return
    }

    const currentUser = { name, email, password }
    if (member) {
      setupUser({
        currentUser,
        endPoint: 'login',
        alertText: 'Acceso realizado con éxito... Redirigiendo',
      })
    } else {
      if (password === passwordRepeat) {
        //contraseñas iguales
        setupUser({
          currentUser,
          endPoint: 'register',
          alertText: 'Nuevo usuario creado... Redirigiendo',
        })

      } else{
        //contraseñas distintas
        displayAlertPass()
        return
      }

    }
  }

  useEffect(() => { //Para un efecto más realista, tardamos 3 segundos en redirigir a la página principal
    if (user) {
      setTimeout(() => {
        navigate('/')
      }, 3000)
    }
  }, [user, navigate])

  return (
    <Wrapper className='full-page'>
      <form className='form' onSubmit={onSubmit}>
        <Link to="/"><Logo /></Link>
        <h3>{values.member ? 'Acceso' : 'Nuevo usuario'}</h3>
        {showAlert && <Alert />}

        {!values.member && (
          <FormRow
            type='text'
            name='name'
            labelText='Nombre'
            value={values.name}
            handleChange={handleChange}
          />
        )}

        <FormRow
          type='email'
          name='email'
          value={values.email}
          handleChange={handleChange}
        />
        {/* password input */}
        <FormRow
          type='password'
          name='password'
          labelText="Contraseña"
          value={values.password}
          handleChange={handleChange}
        />

        {!values.member && (
          <FormRow
            type='password'
            name='passwordRepeat'
            labelText="Repetir Contraseña"
            value={values.passwordRepeat}
            handleChange={handleChange}
          />
        )}
        <button type='submit' className='btn btn-block' disabled={isLoading}>
          Acceder
        </button>
        <p>
          {values.member ? '¿No tienes cuenta?' : '¿Ya eres miembro?'}
          <button type='button' onClick={toggleMember} className='member-btn'>
            {values.member ? '¡Regístrate!' : '¡Loguéate!'}
          </button>
        </p>
        <p><Link to="/recover">Recuperar contraseña</Link></p>
      </form>
    </Wrapper>
  )
}
export default Register
