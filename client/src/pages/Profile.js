import { useState } from 'react'
import { FormRow, Alert } from '../components'
import { useAppContext } from '../context/appContext'
import Wrapper from '../assets/wrappers/DashboardFormPage'
const Profile = () => {
  const { user, showAlert, displayAlert, updateUser } =
    useAppContext()

  const [name, setName] = useState(user?.name)
  const [email, setEmail] = useState(user?.email)
  const [lastName, setLastName] = useState(user?.lastName)
  const [phone, setPhone] = useState(user?.phone)
  const [city, setCity] = useState(user?.city)

  const handleSubmit = (e) => {
    e.preventDefault()
    if (!name || !email || !lastName ) {

      displayAlert()
      return
    }
    updateUser({ name, email, lastName, phone, city})
  }

  return (
    <Wrapper>
      <form className='form' onSubmit={handleSubmit}>
        <h3>Perfil</h3>
        {showAlert && <Alert />}
        <div className='form-center'>
          <FormRow
            type='text'
            name='Nombre'
            value={name}
            handleChange={(e) => setName(e.target.value)}
          />
          <FormRow
            type='text'
            labelText='Apellido'
            name='lastName'
            value={lastName}
            handleChange={(e) => setLastName(e.target.value)}
          />
          <FormRow
            type='number'
            labelText='Teléfono'
            name='phone'
            value={phone}
            handleChange={(e) => setPhone(e.target.value.toString())}
          />
          <FormRow
            type='text'
            labelText='Ciudad'
            name='city'
            value={city}
            handleChange={(e) => setCity(e.target.value)}
          />
          <FormRow
            type='email'
            name='email'
            disabled={true}
            value={email}
            handleChange={(e) => setEmail(e.target.value)}
          />

          <button className='btn btn-block' type='submit'>
            Guardar cambios
          </button>
        </div>
      </form>
    </Wrapper>
  )
}

export default Profile
