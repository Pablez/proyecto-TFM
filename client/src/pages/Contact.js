import { useState } from 'react'
import { FormRow, Alert, FormTextArea } from '../components'
import { useAppContext } from '../context/appContext'
import Wrapper from '../assets/wrappers/DashboardFormPage'
import emailjs from 'emailjs-com'


const Contact = () => {
  const { user, showAlert, displayMessageSending, displayMessageError } =
    useAppContext()

    

  const [name, setName] = useState(user?.name)
  const [email, setEmail] = useState(user?.email)
  const [lastName, setLastName] = useState(user?.lastName)
  const [message, setMessage] = useState("")


  const sendEmail= (e) =>{
    e.preventDefault();

    if (!!message){

      emailjs.sendForm('service_kxgstvh','template_5eqzdla',e.target,'IlFkRYdLgkLA_64wi').then(res=>{
        console.log(res);

        })
        displayMessageSending()
        return
    }else{
      displayMessageError()
    }


    }


  return (
    <Wrapper>
      <form className='form' onSubmit={sendEmail}>
        <h3>Formulario de contacto</h3>
        {showAlert && <Alert />}
        <div className='form-center'>
          <FormRow
            type='text'
            name='Nombre'
            value={name}
            handleChange={(e) => setName(e.target.value)}
          />
          <FormRow
            type='text'
            labelText='Apellido'
            name='lastName'
            value={lastName}
            handleChange={(e) => setLastName(e.target.value)}
          />
          <FormRow

            type='email'
            name='email'
            disabled='disabled'
            value={email}
            handleChange={(e) => setEmail(e.target.value)}
          />
          <FormTextArea
            type='text'
            name='message'
            labelText='Introduce tu consulta'
            value={message}
            handleChange={(e) => setMessage(e.target.value)}
          />

          <button className='btn btn-block' type='submit'>
            Enviar consulta
          </button>
        </div>
      </form>
    </Wrapper>
  )
}

export default Contact
