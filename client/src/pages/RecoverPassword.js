import { useState } from 'react'
import { Logo, FormRow, Alert } from '../components'
import Wrapper from '../assets/wrappers/RegisterPage'
import { useAppContext } from '../context/appContext'
import { Link } from 'react-router-dom'

const RecoverPassword = () => {

  const { user, showAlert, displayAlert, updateUser } =
    useAppContext()

    const [email, setEmail] = useState(user?.email)
    const [password, setPassword] = useState(user?.password)

    const onSubmit = (e) => {
      e.preventDefault()
      if (!email) {
        displayAlert()
        return
      }
      setPassword("")
      updateUser({email, password})
    }


  return (
    <Wrapper className='full-page'>
      <form className='form' onSubmit={onSubmit}>
        <Link to="/"><Logo /></Link>
        <h3>{'Recuperar contraseña'}</h3>
        {showAlert && <Alert />}
        <FormRow
          type='email'
          name='email'
          value={email}
          labelText={"Esta función no se encuentra disponible"}
          disabled={true}
          handleChange={(e) => setEmail(e.target.value)}
        />

        <button  type='submit' className='btn btn-block' disabled={true}>
          Recuperar contraseña
        </button>
        <p>
          <Link to="/" className='member-btn'>
            Regresar
          </Link>
        </p>
      </form>
    </Wrapper>
  )
}
export default RecoverPassword
