import Wrapper from '../assets/wrappers/LandingPage'
import { Logo } from '../components'
import { Link } from 'react-router-dom'
import CarouselImg from '../components/CarouselImg';
import 'react-multi-carousel/lib/styles.css';
import HorizontalBanner from '../components/HorizontalBanner';
import FloatText from '../components/FloatText';
import LandingChart from '../components/LandingChart';
import main from '../assets/images/main22-alt3.svg'


const Landing = () => {

  return (
    <Wrapper>
      <nav>
        <Logo />
        Pablo García García
      </nav>
      <div className='container page'>
        {/* info */}
        <div className='info'>
          <h1>
            Aplicación Web de <span>entrenamiento</span> personal
          </h1>
          <p>
            Crea un nuevo usuario e ingresa tus datos personales y de salud para
            obtener una rutina de actividad física personalizada
            a tus necesidades.
          </p>
          <Link to='/register' className='btn btn-hero'>
            Login/Registro
          </Link>
        </div>
        <CarouselImg />
      </div>
      <div className='line'>
        <HorizontalBanner />
      </div>

      <div className='container page'>
        <img src={main} alt='training' className='img main-img' />
        <div className='info'>
          <h3>
          El porcentaje de personas que no practican actividad física semanal es del 42%
          </h3>
          <LandingChart />
        </div>
        
      </div>
      <div className='containDown'>
        {/*<MountainBackground className="imagenMain" />*/}
        <FloatText></FloatText>
        {/*<Video source={URL} controls={false} autoPlay={true} className="videoMain"></Video>*/}
      </div>
    </Wrapper>
  )
}

export default Landing
