import { Link } from 'react-router-dom'
import img from '../assets/images/notfound.svg'
import Wrapper from '../assets/wrappers/ErrorPage'

const Error = () => {
  return (
    <Wrapper className='full-page'>
      <div>
        <img src={img} alt='Página no encontrada' />
        <h3>Página no encontrada</h3>
        <p>La página a la que intentas acceder no existe</p>
        <Link to='/'>Regresar</Link>
      </div>
    </Wrapper>
  )
}

export default Error
