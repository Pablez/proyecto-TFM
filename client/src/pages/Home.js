
import { ExerciseGrid } from "../components";
import { Search } from "../components";

const Home= () => {
  return (
    <div>
      <Search />
      <ExerciseGrid/>
    </div>
  );
}

export default Home