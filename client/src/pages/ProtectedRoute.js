import { useAppContext } from '../context/appContext'
import { Navigate } from 'react-router-dom'

const ProtectedRoute = ({ children }) => {
  const { user } = useAppContext()
  if (!user) {  //Si no existe usuario registrado en el contexto de la aplicación, llevar a la página de landing
    return <Navigate to='/landing' />
  }
  return children
}

export default ProtectedRoute
