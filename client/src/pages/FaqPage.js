import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import { FaAngleDown } from 'react-icons/fa/index.esm';
import TextFaqs from '../components/TextFaqs';


const FaqPage = () => {

  //BIBLIOGRAFIA: https://es.calcuworld.com/calculadora-nutricional/calculadora-de-calorias-harris-benedict/
  //https://www.who.int/es/news-room/fact-sheets/detail/physical-activity

  
  return (
    <>
      <Accordion style={{ maxWidth: "600px", margin:"0 auto" }}>
        <AccordionSummary
          expandIcon={<FaAngleDown color={"white"} />}
          style={{ color: "white", backgroundColor: "#17034e" }}
        >
          <h5>Por qué realizar actividad física</h5>
        </AccordionSummary>
        <AccordionDetails>
          <TextFaqs param={0} />
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ maxWidth: "600px", margin:"0 auto" }}>
        <AccordionSummary
          expandIcon={<FaAngleDown color={"white"} />}
          style={{ color: "white", backgroundColor: "#17034e" }}
        >
          <h5>Cómo funciona la calculadora</h5>
        </AccordionSummary>
        <AccordionDetails>
          <TextFaqs param={1} />
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ maxWidth: "600px", margin:"0 auto" }}>
        <AccordionSummary
          expandIcon={<FaAngleDown color={"white"} />}
          style={{ color: "white", backgroundColor: "#17034e" }}
        >
          <h5>Recomendaciones de actividad física</h5>
        </AccordionSummary>
        <AccordionDetails>
          <TextFaqs param={2} />
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ maxWidth: "600px", margin:"0 auto" }}>
        <AccordionSummary
          expandIcon={<FaAngleDown color={"white"} />}
          style={{ color: "white", backgroundColor: "#17034e" }}
        >
          <h5>Cómo ponerse en contacto con nosotros</h5>
        </AccordionSummary>
        <AccordionDetails>
          <TextFaqs param={3} />
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ maxWidth: "600px", margin:"0 auto" }}>
        <AccordionSummary
          expandIcon={<FaAngleDown color={"white"} />}
          style={{ color: "white", backgroundColor: "#17034e" }}
        >

          <h5>Cómo eliminar mi cuenta</h5>
        </AccordionSummary>
        <AccordionDetails>
          <TextFaqs param={4} />
        </AccordionDetails>
      </Accordion>
    </>
  )
}

export default FaqPage
