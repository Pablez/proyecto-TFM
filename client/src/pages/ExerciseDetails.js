import { exercisesSeguridad } from "../utils/exercises.json" //en caso de fallo al recuperar datos de API
import styles from "../assets/modules/ExerciseDetails.module.css"
import { Link, useParams } from "react-router-dom"
import React, { useEffect, useState } from "react"
import Video from "../components/Video"
import { FaArrowLeft } from "react-icons/fa"
import { FaArrowRight, FaBackward, FaHome } from "react-icons/fa/index.esm"

const ExerciseDetails = () => {
    //https://www.mockable.io/a/#/space/demo6197451/rest/QwaHQ-AAA
    const URI = `http://demo6197451.mockable.io/`

    const [ exercises, setExercises ] = useState(null);
    const { exerciseId } = useParams();

    useEffect(() => {
        fetch(URI, {
            mode: "no-cors",
})
        .then((res) => res.json())
        .then((data) => (
            setExercises(data.exercises)))
            .catch((e) => {
                setExercises(exercisesSeguridad)
            });
    }, [exerciseId]);

    if (!exercises) return null;

    var exercise = (exercises[exerciseId-1])

    return (

        
        <div>
            <div className={styles.title}>
                <h1>
                    <strong>{exercise.nombre} ({exercise.duracion})</strong>
                </h1>
            </div>

            <div className={styles.detailsContainer}>
                <div className={`${styles.col} ${styles.videoCont}`}>
                    <Video className={styles.video} source={exercise.video} autoplay={false}/>
                </div>

                <div className={styles.col + " " + styles.exerciseDetails}>
                    <h4>
                        <strong>Grupos musculares involucrados:</strong>
                    </h4>
                    <ul className={styles.list}>
                        {exercise.grupoMuscular.map((muscle, i) => (
                            <li key={i}>{muscle}</li>
                        ))}
                    </ul>
                    <h4><strong>Descripción: </strong></h4> <p>{exercise.descripcion}</p>
                    <h4><strong>Necesita material: </strong>{exercise.material}</h4>
                    <h4><strong>Dificultad: </strong>{exercise.dificultad}</h4>
                </div>

            </div>
            <div className={styles.btnContainer}>
                {exercise.id !==1 ? (
                    <div className={styles.btn}><Link to={`/rutina/${exerciseId-1}`}><FaArrowLeft className={styles.icon} /> <span>Anterior</span></Link></div>
                ):(
                    <></>
                )}
                
                <div className={styles.btn}><Link to="/" ><FaHome className={styles.icon} /> <span>Home</span></Link></div>
                {exercise.id === exercises.length ? (
                    <div className={styles.btn}><Link to={`/rutina/1`}><FaBackward className={styles.icon} /><span> Primera</span></Link> </div>
                ):(
                    <div className={styles.btn}><Link to={`/rutina/${parseInt(exerciseId)+1}`}><span>Siguiente </span><FaArrowRight className={styles.icon} /></Link> </div>
                )}
            </div>
        </div>
    )
}

export default ExerciseDetails