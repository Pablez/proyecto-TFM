import Landing from './Landing'
import Error from './Error'
import Register from './Register'
import ProtectedRoute from './ProtectedRoute'
import Profile from "./Profile"
import SharedLayout from "./SharedLayout"
import ExerciseDetails from './ExerciseDetails'
import RecoverPassword from './RecoverPassword'
import Contact from './Contact'
import FaqPage from './FaqPage'
import Home from './Home'
export { ExerciseDetails, Landing, Error, Register, ProtectedRoute, Profile, SharedLayout, RecoverPassword, Contact, FaqPage, Home }
