import express from 'express'
const app = express()
import dotenv from 'dotenv'
dotenv.config()
import 'express-async-errors'
import morgan from 'morgan'

import { dirname } from 'path'
import { fileURLToPath } from 'url'
import path from 'path'

import helmet from 'helmet'
import xss from 'xss-clean'
import mongoSanitize from 'express-mongo-sanitize'

import connectDB from './db/connect.js'

// routers
import authRouter from './routes/authRoutes.js'

// middleware
import notFoundMiddleware from './middleware/not-found.js'
import errorHandlerMiddleware from './middleware/error-handler.js'

if (process.env.NODE_ENV !== 'production') {
  app.use(morgan('dev'))
}

const __dirname = dirname(fileURLToPath(import.meta.url))

// PARA DESPLEGAR
app.use(express.static(path.resolve(__dirname, './client/build')))

app.use(express.json())
app.use(helmet()) //Añade varias cabeceras HTTP para securizar las aplicaciones express
app.use(xss()) // "Saneamos" los inputs, añadimos seguridad
app.use(mongoSanitize()) //Evita Injections en mongoDB

app.use('/api/v1/auth', authRouter)

// ATENCION PARA DESPLEGAR
app.get('*', (req, res) => {
res.sendFile(path.resolve(__dirname, './client/build', 'index.html'))
})

app.use(notFoundMiddleware)
app.use(errorHandlerMiddleware)

const port = process.env.PORT || 5000

const start = async () => {
  try {
    await connectDB(process.env.MONGO_URL)
    app.listen(port, () => {
      console.log(`Servidor escuchando en puerto ${port}...`)
    })
  } catch (error) {
    console.log(error)
  }
}

start()
