import { NoAthenticatedError } from '../errors/index.js';

const checkPermissions = (requestUser, resourceUserId) => {
  if (requestUser.userId === resourceUserId.toString()) {
    return
  }

  throw new NoAthenticatedError('No estás autorizado para esta ruta')
}

export default checkPermissions
