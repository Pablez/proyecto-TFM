import mongoose from 'mongoose'
import validator from 'validator'
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Por favor, añade nombre'],
    minlength: 3,
    maxlength: 20,
    trim: true,
  },
  email: {
    type: String,
    required: [true, 'Por favor, añade email'],
    validate: {
      validator: validator.isEmail,
      message: 'Por favor, añade email válido',
    },
    unique: true,
  },
  password: {
    type: String,
    required: [true, 'Por favor, añade contraseña'],
    minlength: 6,
    select: false,
  },
  lastName: {
    type: String,
    trim: true,
    maxlength: 20,
    default: 'Apellido',
  },
  phone: {
    type: String,
    trim: true,
    default: 'Teléfono',
  },
  city: {
    type: String,
    trim: true,
    maxlength: 20,
    default: 'Ciudad',
  },
})

UserSchema.pre('save', async function () {
  // codificamos el password con la librería bcrypt para que no sea visible
  if (!this.isModified('password')) return
  const salt = await bcrypt.genSalt(10)  //fragmento aleatorio que se usará para generar el hash asociado a la password, y se guardará junto con ella en la base de datos
  this.password = await bcrypt.hash(this.password, salt) //une la contraseña encriptada con el hash aleatorio para conseguir una password encriptada única
})

UserSchema.methods.createJWT = function () {
  //Creamos un token con librería JWT para comprobar los inicios de sesión
  return jwt.sign({ userId: this._id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_LIFETIME,
  })
}

UserSchema.methods.comparePassword = async function (candidatePassword) {
  const isMatch = await bcrypt.compare(candidatePassword, this.password) //método para comparar la contraseña en el frontal con la guardada en laa base de datos
  return isMatch
}

export default mongoose.model('User', UserSchema)
