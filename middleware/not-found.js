const notFoundMiddleware = (req, res) =>
  res.status(404).send('La ruta no existe')

export default notFoundMiddleware
