import jwt from 'jsonwebtoken'
import { NoAthenticatedError } from '../errors/index.js'

NoAthenticatedError
const auth = async (req, res, next) => {
  const authHeader = req.headers.authorization
  if (!authHeader || !authHeader.startsWith('Bearer')) {
    throw new NoAthenticatedError('Autenticación inválida')
  }
  const token = authHeader.split(' ')[1]
  try {
    const payload = jwt.verify(token, process.env.JWT_SECRET)
    req.user = { userId: payload.userId }

    next()
  } catch (error) {
    throw new NoAthenticatedError('Autenticación inválida')
  }
}

export default auth
